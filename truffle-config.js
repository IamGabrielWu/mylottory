const HDWalletProvider = require("truffle-hdwallet-provider");
module.exports = {
  // See <http://truffleframework.com/docs/advanced/configuration>
  // to customize your Truffle configuration!
  // contracts_build_directory: "./output",
  networks: {
    rinkeby: {
      provider: new HDWalletProvider(process.env.twelvewords, "https://rinkeby.infura.io/"+process.env.infuraapi),
      network_id: '23',
    },
    ropsten: {
      provider: new HDWalletProvider(process.env.twelvewords, "https://ropsten.infura.io/"+process.env.infuraapi),
      network_id: '23'
    },
    kovan: {
      provider: new HDWalletProvider(process.env.twelvewords, "https://kovan.infura.io/"+process.env.infuraapi),
      network_id: '23',
    },
    dev: {
      host: "127.0.0.1",
      port: 7545,
      network_id: "5777" // match any network
    },
  }
};
