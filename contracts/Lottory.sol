pragma solidity ^0.4.19;
contract Lottory {
    address private manager;
    address[] private players;
    mapping(address=>Player) playermap;
    // event logging(address p);
    uint private startlottory;
    uint public endlottory;

    event contractbalance(
        uint poolprize,
        uint enddate
    );
    struct Player{
        address playerAdd;
        uint  money;
    }

    function Lottory() public {
        manager=msg.sender;
        if(address(this).balance==0){
          startlottory=block.timestamp;
          endlottory=startlottory+86400;
          contractbalance(0,endlottory);
        }
    }

    function getPlayers() view public returns (address[]) {
        return players;
    }

    /* function enter() public payable enteronlyonce{ */
    function enter() public payable{
        //to do need to avoid a user repeatedly enter
        require(msg.value > 0.01 ether);
        players.push(msg.sender);
        playermap[msg.sender]=Player(msg.sender,msg.value);
        contractbalance(address(this).balance,endlottory);
        if(players.length>99){
          pickWinner();
        }
    }

    function random() private view returns (uint) {
        //sha3 is also a global function
        //block is global variable that we can access any time
        //now is also global
        return uint(keccak256(block.difficulty, now, players));
    }

    function pickWinner() private restricted{
        uint index=random()%players.length;
        players[index].transfer(address(this).balance);
        // new address[](0) this way we create a brand new dynamic array, that has 0 size.
        // if we new address[](5) this way we create a brand new dynamic array, that has size 5.
        //if we want to create a fixed size array, then like this new address[5](0);
        players=new address[](0);
        startlottory=block.timestamp;
        endlottory=startlottory+86400;
        contractbalance(address(this).balance,endlottory);
    }

    modifier restricted(){
        require(msg.sender==manager);
        _;
    }

    modifier enteronlyonce(){
        require(playermap[msg.sender].playerAdd == 0x0000);
        _;
    }


}
