var Migrations = artifacts.require("Migrations");
var Lottory = artifacts.require("Lottory");
module.exports = function(deployer) {
  deployer.deploy(Migrations);
  deployer.deploy(Lottory);
};
