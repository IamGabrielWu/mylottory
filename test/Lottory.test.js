const Lottory = artifacts.require("Lottory");
let lottory;
beforeEach(async()=>{
  lottory = await Lottory.deployed();
})

contract('Lottory', (accounts)=>{
  it('deploys a contract', ()=>{
      assert.ok(lottory.address);
  });
  it('allows one account to enter', async ()=>{
      await lottory.enter.sendTransaction({
        from: accounts[0],
        value: web3.toWei('0.02','ether'),
        gas: 410085
      });
      const players = await lottory.getPlayers.call({
        from: accounts[0]
      });
      assert.equal(accounts[0], players[0]);
      assert.equal(1,players.length);
  });
  it('not allow one account repeatedly enter', async ()=>{
    //within every it block, contract instance is completely new. so here two times of enter. above enter within above it block is not counted.
    try{
      await lottory.enter.sendTransaction({
        from: accounts[0],
        value: web3.toWei('0.02','ether'),
        gas: 410085
      });
      await lottory.enter.sendTransaction({
        from: accounts[0],
        value: web3.toWei('0.02','ether'),
        gas: 410085
      });
      assert(false);
    }catch(err){
      assert(err);
    }
  });
  it('only manager can call pick winner', async ()=>{
    try{
      await lottory.enter.sendTransaction({
        from: accounts[0],
        value: web3.toWei('0.02','ether'),
        gas: 410085
      });
      await lottory.enter.sendTransaction({
        from: accounts[1],
        value: web3.toWei('0.02','ether'),
        gas: 410085
      });
      await lottory.enter.sendTransaction({
        from: accounts[2],
        value: web3.toWei('0.02','ether'),
        gas: 410085
      });
      await lottory.enter.sendTransaction({
        from: accounts[3],
        value: web3.toWei('0.02','ether'),
        gas: 410085
      });
      await lottory.pickWinner.sendTransaction({
        from: accounts[1],
        gas: 410085
      });
      assert(false)
    }catch(err){
      assert(err);
    }
  });
})
